# -*- coding: utf-8 -*-
import codecs
import nltk
import re
import unicodedata
from collections import Counter


def load_file(filename):
    lines = codecs.open(filename, encoding="utf-8").readlines()
    return [line.replace("\n", "") for line in lines]


def create_text(sentences):
    text = ""
    for i in range(len(sentences)):
        text += " " + sentences[i]
    return text


def words_from(filename):
    lines = load_file(filename)
    text = create_text(lines)
    words = nltk.word_tokenize(text)
    return words

def get_tuples(filename):
    entities_tuples = []
    sentences = load_file(filename)
    for sent in sentences:
        tuples = []
        words = sent.split(" ")
        for word in words:
            split = word.split("|")
            tuples.append((split[0], split[1]))

        entities_tuples.append(tuples)

    return entities_tuples

def save_frequency_region(sentences):
    region_regex = re.compile(".*,?\sregi(o|\w)n\s(?!metropolitana).*$", flags=re.UNICODE | re.IGNORECASE)
    filter_sentences = [s for s in sentences if region_regex.search(s)]
    f = codecs.open("./reverse_addresses/frequency_region.csv", "w", encoding="utf-8")
    f2 = codecs.open("./reverse_addresses/list_of_addresses_regions.txt", "w", encoding="utf-8")
    f.write("RM\t%s\n" % (len(sentences) - len(filter_sentences)))
    f.write("NOT RM\t%s\n" % (len(filter_sentences)))
    for i in range(len(filter_sentences)):
        tokens = filter_sentences[i].split(",")
        for j in range(len(tokens)):
            if region_regex.search(tokens[j]):
                f2.write("%s\n" % tokens[j])

    f.close()
    f2.close()


def save_metropolitan_district_frequency(sentences):
    region_regex = re.compile(".*,?\sregi(o|\w)n\smetropolitana.*$", flags=re.UNICODE | re.IGNORECASE)
    filter_sentences = [s for s in sentences if region_regex.search(s)]
    f = codecs.open("./reverse_addresses/list_of_addresses_districts_RM.txt", "w", encoding="utf-8")
    for i in range(len(filter_sentences)):
        tokens = filter_sentences[i].split(",")
        for j in range(len(tokens)):
            if j + 1 < len(tokens) and region_regex.search(tokens[j + 1]):
                f.write("%s\n" % tokens[j])

    f.close()


def word_shapes(tokens, exclude_characters=list()):
    uppercase_re = re.compile(r"[A-ZÁÉÍÓÚÑÜÀÈÌÒÙ]", flags=re.UNICODE)
    lowercase_re = re.compile(r"[a-záéíóúüñàèìòù]", flags=re.UNICODE)
    shapes = list()
    for token in tokens:
        newToken = re.sub(uppercase_re, "A", token)
        newToken = re.sub(lowercase_re, "a", newToken)
        newToken = re.sub(r"[0-9]", "d", newToken)
        if len(exclude_characters) > 0:
            raw_regex = r"[^a-záéíóúüñàèìòùA-ZÁÉÍÓÚÑÜÀÈÌÒÙ0-9"
            for char in exclude_characters:
                raw_regex += "\\" + char

            raw_regex += "]"
            newToken = re.sub(raw_regex, "?", newToken)
        else:
            newToken = re.sub(r"[^a-záéíóúüñàèìòùA-ZÁÉÍÓÚÑÜÀÈÌÒÙ0-9]", "?", newToken)
        shapes.append(newToken)

    return shapes


def compact_word_shapes(tokens, exclude_characters):
    compact_shapes = list()
    shapes = word_shapes(tokens, exclude_characters)
    for shape in shapes:
        newShape = re.sub(r"AA+", "A", shape)
        newShape = re.sub(r"aa+", "a", newShape)
        newShape = re.sub(r"dd+", "d", newShape)
        newShape = re.sub(r"\?\?+", "?", newShape)
        compact_shapes.append(newShape)

    return compact_shapes


def get_shapes_freq(filename, characters):
    lines = load_file(filename)
    tokens = list()
    for line in lines:
        new_line = re.sub(r"\n", "", line)
        tokens += new_line.split(" ")

    shapes = word_shapes(tokens, characters)
    compact_shapes = compact_word_shapes(tokens, characters)

    fdist1 = nltk.FreqDist(shapes)
    fdist2 = nltk.FreqDist(compact_shapes)

    shapes_items = fdist1.items()
    compact_items = fdist2.items()

    sorted_shapes = sorted(shapes_items, key=lambda tup: tup[1], reverse=True)
    sorted_compact = sorted(compact_items, key=lambda tup: tup[1], reverse=True)

    return len(tokens), sorted_shapes, sorted_compact


def save_words_with_special_characters(dest_filename, tokens, characters):
    f = codecs.open(dest_filename, "w", encoding="utf-8")
    escaped_chars = []

    for char in characters:
        escaped_chars.append("\\" + char if char != "\\" else "\\\\")

    str_regex = "|".join(escaped_chars)
    str_regex = "(" + str_regex + ")" if len(characters) > 1 else str_regex
    pattern = re.compile(str_regex, flags=re.UNICODE)
    tokens = [token for token in tokens if pattern.search(token) and token not in "".join(characters)]

    for token in tokens:
        f.write("%s\n" % token)

    f.close()


def save_patterns_with_special_characters(src_filename, dest_filename, character):
    lines = load_file(src_filename)
    tokens = []
    for line in lines:
        tokens += line.split(" ")

    save_words_with_special_characters(dest_filename, tokens, [character])


def save_word_patterns(src_filename, dest_filename, characters):
    """

    Method for save a report of the 10 most common word shapes in the src file

    :param src_filename: file path that contain a list of addresses
    :param dest_filename: file path + file name to save the report
    :param characters: List of characters for observation, any character outside this list will be
    replaced with "?" in the patterns.
    :return: None
    """

    total, shapes, compact_shapes = get_shapes_freq(src_filename, characters)
    f = codecs.open(dest_filename, "w", encoding="utf-8")
    f.write("Total\t%d\n" % total)
    f.write("\n")
    f.write("Shapes\n")
    f.write("========================================\n")
    f.write("\n")
    outer_limit = 10 if len(shapes) > 10 else len(shapes)
    for i in range(outer_limit):
        f.write("{}".format(shapes[i][0]))
        for j in range(40 - len(shapes[i][0])):
            f.write(" ")
        f.write("{}\n".format(shapes[i][1]))
    f.write("\n")
    f.write("Compact Shapes\n")
    f.write("========================================\n")
    outer_limit = 10 if len(compact_shapes) > 10 else len(compact_shapes)
    for i in range(outer_limit):
        f.write("{}".format(compact_shapes[i][0]))
        for j in range(40 - len(compact_shapes[i][0])):
            f.write(" ")
        f.write("{}\n".format(compact_shapes[i][1]))
    f.write("\n")
    f.close()


def strip_accents(text):
    """
    Strip accents from input String.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    try:
        # unicode is a default on python 3
        text = unicode(text, 'utf-8')
    except (TypeError, NameError):
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)


def count_words_from_dictionary(filepath):
    count = 0
    lines = load_file(filepath)
    for line in lines:
        count += len(line.split("|"))
    return count


def dataset_statistics(filepath):
    cnt = Counter()
    lines = load_file(filepath)
    total_instances = len(lines)
    total_entities = 0
    total_tokens = 0
    for line in lines:
        segments = line.split(" ")
        total_tokens += len(segments)
        for segment in segments:
            # Rules
            match = re.search(r"\|(B_([^,\s.$]{1,3})|O)(\s|,|\.|$)", segment)
            if match:
                entity_class = match.group(2) if match.group(2) else match.group(1)
                cnt[entity_class] += 1
                total_entities = total_entities + 1 if entity_class != 'O' else total_entities

    print("Statistics for: {}".format(filepath))
    print("____________________________________________")
    print("instances: {}    entities: {}    tokens: {}".format(total_instances, total_entities, total_tokens))
    print("")
    print("")
    print("Entity           Support")
    print("==========================")
    for item in cnt.items():
        print(" {}              {}".format(item[0], item[1]))


def get_entities(sentences, entity_type):
    entities = []
    for sentence in sentences:
        words = nltk.wordpunct_tokenize(sentence)
        for i, word in enumerate(words):
            if word == entity_type:
                entities.append(words[i - 2])
    return entities


def get_sentences(filepath):
    lines = load_file(filepath)
    sentences = []
    for line in lines:
        sentence = []
        words_and_labels = line.split()
        if len(words_and_labels) > 0:
            for word_and_label in words_and_labels:
                words = word_and_label.split("|")
                if len(words) == 2 and words[0]:
                    sentence.append(words[0])

        if len(sentence) > 0:
            sentences.append(" ".join(sentence))

    return sentences