import pandas as pd

if __name__ == "__main__":
    column_names = ['code', 'name', 'qty', 'region_code', 'region']

    ngdf = pd.read_csv("./data/raw/district_frequency_(not_geo).csv", names=column_names, sep=";")
    gdf = pd.read_csv("./data/raw/district_frequency.csv", names=column_names, sep=";")

    ngdf = ngdf.set_index('region_code')
    ngdf = ngdf.groupby(['region_code', 'region'])['qty'].agg('sum').reset_index()
    ngdf = ngdf[['region', 'qty']]
    ngdf2 = ngdf.sort_values(by='qty', ascending=False)
    ngdf2.to_csv("./data/raw/reqions_qty__not_geo.csv", sep="\t", encoding="utf-8")

    gdf = gdf.set_index('region_code')
    gdf = gdf.groupby(['region_code', 'region'])['qty'].agg('sum').reset_index()
    gdf = gdf[['region', 'qty']]
    gdf2 = gdf.sort_values(by='qty', ascending=False)

    gdf2.to_csv("./data/raw/reqions_qty_geo.csv", sep="\t", encoding="utf-8")