Total	510

Shapes
========================================

#dddd?                                  145
#dddd                                   125
#ddd                                    68
#ddd?                                   56
#dd                                     19
#ddddd                                  18
#dd?                                    12
#ddddd?                                 11
#dd?dd?                                 5
AAAAAAA#dddd                            3

Compact Shapes
========================================
#d                                      232
#d?                                     228
A#d                                     8
A#d?                                    8
#d?d?                                   5
#d?Aa                                   4
A#                                      3
#d?d                                    3
#d?A                                    2
#d?a?a                                  2

