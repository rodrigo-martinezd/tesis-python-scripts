# -*- coding: utf-8 -*-
import fiona
import joblib
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
from shapely.geometry import shape

if __name__ == "__main__":
    with fiona.open("./data/gis/Comuna.shp") as map:
        try:
            counter = joblib.load("./data/quantity_of_points_in_every_district.out")
        except Exception:
            raise Exception("missing file './data/quantity_of_points_in_every_district.out'")

        # Build data
        rows = []
        codes = []
        vmax = max([count for code, count in counter.items() if len(code) == 5 and code[:2] == '13'])
        vmin = min([count for code, count in counter.items() if len(code) == 5 and code[:2] == '13'])
        print(vmax)
        print(vmin)
        for key, item in map.items():
            codes.append(item['properties']['COMUNA'])
            rows.append({
                'code': item['properties']['COMUNA'],
                'name': item['properties']['DESC_COMUN'],
                'points_qty': counter[item['properties']['COMUNA']],
                'geometry': shape(item['geometry'])
            })

        rm_rows = list(filter(lambda x: len(x['code']) == 5 and x['code'][:2] == '13', rows))
        rm_codes = list(filter(lambda x: len(x) == 5 and x[:2] == '13', codes))

        df = pd.DataFrame(rm_rows, index=[rm_codes], columns=['code', 'name', 'points_qty', 'geometry'])
        gdf = gpd.GeoDataFrame(df, geometry='geometry')
        fig, ax = plt.subplots()
        ax.set_aspect('equal')
        gdf.plot(column='points_qty', cmap='Blues', linewidth=0.5, edgecolor='0.5', ax=ax)
        ax.axis('off')

        sm = plt.cm.ScalarMappable(cmap='Blues', norm=plt.Normalize(vmax=vmax, vmin=vmin))
        sm._A = []
        cbar = fig.colorbar(sm)
        fig.savefig("./data/images/rm_districts_coords_qty.png", dpi=200, bbox_inches='tight')
        plt.show()
