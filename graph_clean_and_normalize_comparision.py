import matplotlib.pyplot as plt


if __name__ == "__main__":

    fig = plt.figure()

    nombres1 = ['Raw', 'Cleaned', 'Normalized']
    datos1 = [63.25, 74.25, 95.75]
    color1 = ['#27ae60', '#e67e22', '#8e44ad']
    xx1 = range(len(datos1))

    plt.bar(xx1, datos1, width=0.3, align='center', color=color1)
    plt.xticks(xx1, nombres1)
    plt.ylabel("Addresses Geocoded %")
    for i, v in enumerate(datos1):
        plt.text(i - .125, v + 1, str(v) + '%', color='black')

    plt.savefig("/data/out/clean_and_normalization_comparision.png", dpi=300, bbox_inches='tight')
