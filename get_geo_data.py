# -*- coding: utf-8 -*-
import fiona
import joblib
from collections import Counter
from shapely.geometry import Point, shape

if __name__ == "__main__":
    with fiona.open("./data/gis/Comuna.shp") as map:
        multipolygons = []
        evaluated_points = 0
        evaluated_polygons = 0
        counter = Counter()
        district_keymap = dict()

        try:
            coordinates = joblib.load("./data/gis/addresses_coordinates.out")
        except Exception:
            raise Exception("missing file '/data/gis/addresses_coordinates.out'")

        try:
            multipolygons = joblib.load("./data/gis/polygons.geometry")
            print("Load district polygons from file successful!")
        except Exception:
            # Build polygons
            print("Start to building polygons...\n")
            for k, item in map.items():
                district_bounds = []
                evaluated_polygons += 1
                print("Building MultiPolygon for: {}".format(item['properties']['DESC_COMUN']))
                print("Polygons: {}/{}".format(evaluated_points, len(item['geometry']['coordinates'])))
                multipolygon = shape(item['geometry'])
                multipolygons.append((item['properties']['COMUNA'], multipolygon))
                district_keymap[item['properties']['COMUNA']] = {
                    'name': item['properties']['DESC_COMUN'],
                    'region_name': item['properties']['DESC_REGIO'],
                    'region_code': item['properties']['REGION']
                }

            joblib.dump(multipolygons, "./data/gis/polygons.geometry")
            print("Polygons created!\n")

        print("Start to check points in polygons...")

        # Check if points belongs to a polygon
        for address in coordinates:
            evaluated_points += 1
            print("Points {}/{}".format(evaluated_points, len(coordinates)))

            # Invert order because shapefile come in format (long, lat)
            point = Point(float(address['longitude']), float(address['latitude']))

            for district_code, district_polygon in multipolygons:
                if district_polygon.contains(point):
                    counter[district_code] += 1
                    break

        joblib.dump(counter, "./data/quantity_of_points_in_every_district.out")
        f = open("./data/raw/district_frequency.csv", "w", encoding="utf-8")

        for district_code, count in counter.items():
            name = district_keymap[district_code]['name']
            region = district_keymap[district_code]['region_name']
            region_code = district_keymap[district_code]['region_code']
            f.write("{};{};{};{};{}\n".format(district_code, name, count, region_code, region))

        f.close()
