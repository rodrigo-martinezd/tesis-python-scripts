import nltk
from nltk.tokenize import word_tokenize
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from norman import load_file
from string import punctuation


def filter_chars(words, skip_chars=[]):
    chars = []
    for sent in words:
        for word in word_tokenize(sent):
            if len(word) == 1 and word in punctuation and word not in skip_chars:
                chars.append(word)

    return chars

if __name__ == "__main__":
    
    addresses_not_geocoded = load_file("./addresses_without_georreferences.txt")
    addresses_geocoded = load_file("./addresses_with_georreferences.txt")
    addresses_gmaps = load_file("./addresses_reverse_geocoded.txt")

    not_geocoded_chars = filter_chars(addresses_not_geocoded)
    geocoded_chars = filter_chars(addresses_geocoded)
    gmaps_chars = filter_chars(addresses_gmaps)

    not_geocoded_freq = nltk.FreqDist(not_geocoded_chars)
    geocoded_freq = nltk.FreqDist(geocoded_chars)
    gmaps_freq = nltk.FreqDist(gmaps_chars)

    most_common_not_geocoded = not_geocoded_freq.most_common(20)
    most_common_geocoded = geocoded_freq.most_common(20)
    most_common_gmaps = gmaps_freq.most_common(20)


    # Create graphs
    gs = gridspec.GridSpec(2, 2)

    fig = plt.figure()
    fig.subplots_adjust(hspace=0.15, wspace=0.45, left=0.15, bottom=0.07, right=0.97, top=0.97)
    ax1 = fig.add_subplot(gs[0, 0])
    ax1.barh(range(len(most_common_geocoded)), [val[1] for val in most_common_geocoded], align='center', color='#3498db', linewidth=0.3)
    ax1.set_yticks(range(len(most_common_geocoded)))
    ax1.set_yticklabels([val[0] for val in most_common_geocoded])
    ax1_patch = mpatches.Patch(color='#3498db', label='Addresses geocoded')
    ax1.legend(handles=[ax1_patch], fontsize=8)
    ax1.tick_params(labelsize=8)

    ax2 = fig.add_subplot(gs[0, 1])
    ax2.barh(range(len(most_common_not_geocoded)), [val[1] for val in most_common_not_geocoded], align='center', color='#e67e22', linewidth=0.3)
    ax2.set_yticks(range(len(most_common_not_geocoded)))
    ax2.set_yticklabels([val[0] for val in most_common_not_geocoded])
    ax2_patch = mpatches.Patch(color='#e67e22', label='Addresses not geocoded')
    ax2.legend(handles=[ax2_patch], fontsize=8)
    ax2.tick_params(labelsize=8)

    ax3 = fig.add_subplot(gs[1, :])
    ax3.barh(range(len(most_common_gmaps)), [val[1] for val in most_common_gmaps], align='center', color='#1abc9c', linewidth=0.3)
    ax3.set_yticks(range(len(most_common_gmaps)))
    ax3.set_yticklabels([val[0] for val in most_common_gmaps])
    ax3_patch = mpatches.Patch(color='#1abc9c', label='Addresses reverse geocoded')
    ax3.legend(handles=[ax3_patch], fontsize=8)
    ax3.tick_params(labelsize=8)

    plt.rc('axes', labelsize=10)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=8)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=8)  # fontsize of the tick labels
    plt.rc('legend', fontsize=8)
    fig.savefig("./data/images/most_common_chars.png", dpi=300, bbox_inches='tight')

    gmaps_chars = filter_chars(addresses_gmaps, skip_chars=[".", ","])
    gmaps_freq = nltk.FreqDist(gmaps_chars)
    most_common_gmaps = gmaps_freq.most_common(50)

    fig2 = plt.figure(2)
    plt.barh(range(len(most_common_gmaps)), [val[1] for val in most_common_gmaps], align='center', color='#1abc9c',
             linewidth=0.3)
    plt.yticks(range(len(most_common_gmaps)), [val[0] for val in most_common_gmaps])
    fig2_patch = mpatches.Patch(color='#1abc9c', label='Addresses reverse geocoded')
    plt.legend(handles=[fig2_patch])
    plt.tick_params(labelsize=8)
    fig2.savefig("./data/images/most_common_chars_reverse_extended.png", dpi=300, bbox_inches='tight')
    plt.show()