import joblib
import json
import numpy as np
from string import Template
import re
from nltk.corpus import stopwords
from jellyfish import jaro_winkler
from norman import strip_accents


chilean_address_template = Template("$Street_type $Street_name $Street_number, $City_district, $Region, $Country")

mapping_class = {
    "ST": "Street_type",
    "S": "Street_name",
    "SN": "Street_number",
    "RO": "Road",
    "RON": "Road_number",
    "CTD": "City_district",
    "CT": "City",
    "R": "Region",
    "C": "Country",
}

classes_excluded = [
    "U",
    "UN",
    "F",
    "PN",
    "L",
    "Z",
    "N",
    "PV",
    "PVC",
    "O",
]

def calculate_order_measure(src_lst, test_lst):
    """
    Calculate the percentage of source entities that match in  same order that the test list

    :param src_lst: list of tuples (word, entity_class) where entity_class is one of
     ["Street_name", "City_district", ...]
    :param test_lst: list of tuples (word, entity_class) where entity_class is one of
     ["Street_name", "City_district", ...]
    :return: percentage of matches
    """
    allowed_classes = [
        "Street_type", "Street_name", "Street_number", "City_district", "Region", "Country",
        "City"
    ]
    n_hits = 0
    total_hits = len([_class for segment, _class in src_lst if _class in allowed_classes])
    src_labels = [_class for segment, _class in src_lst]
    test_labels = [_class for segment, _class in src_lst]

    for label in src_labels:
        try:
            index = test_labels.index(label)
            n_hits += 1
            if index > len(test_labels) - 1:
                test_labels = test_labels[(index + 1):]
        except:
            pass

    return n_hits / total_hits

def calculate_completeness_measure(src_lst, test_lst):
    """
    Calculate the percentage of entities of the src list contained in the test

    :param src_lst: list of tuples (word, entity_class) where entity_class is one of
     ["Street_name", "City_district", ...]
    :param test_lst: list of tuples (word, entity_class) where entity_class is one of
     ["Street_name", "City_district", ...]
    :return: percentage of matches
    """
    count = 0
    test_entities = [entity for word, entity in test_lst]

    for word, entity in src_lst:
        if entity in test_entities:
            count += 1

    return count / len(test_lst)


def calculate_similarity(src_lst, test_lst):
    """
    Calculate the mean of every match of entities in src and test lists

    :param src_lst: list of tuples (word, entity_class) where entity_class is one of
     ["Street_name", "City_district", ...]
    :param test_lst: list of tuples (word, entity_class) where entity_class is one of
     ["Street_name", "City_district", ...]
    :return: the mean of the jaro winkler distance of every pair of matches entities
    """
    similarities = []
    for word, src_class in src_lst:
        filtered_tupples = [(word, _class) for word, _class in test_lst if _class == src_class]
        if len(filtered_tupples) > 0:
            similarities.append(jaro_winkler(word, filtered_tupples[0][0]))

    np_similarities = np.asarray(similarities, dtype=np.float32)
    return np.mean(np_similarities, dtype=np.float32)


def calculate_address_similarity(src_lst, test_lst):
    order = calculate_order_measure(src_lst, test_lst)
    completeness = calculate_completeness_measure(src_lst, test_lst)
    word_similarity = calculate_similarity(src_lst, test_lst)
    address_similarity = (1/5)*(2*order + 2*completeness + word_similarity)

    return address_similarity, order, completeness, word_similarity


def test_standardization(src_examples, test_examples, dest_file=""):
    """
    Calculate the Order, Completeness, Similarity and the Address Similarity between
    two sets of predictions a src list of examples of our system and a list of test
    examples from google maps reverse geocoded

    :param src_examples: List of examples predictions tagged where each example is in the for of
    [(w1, class1), (w2, class2),..., (wn, classn)] where class is one of ["B_S", "B_ST", "I_S", "B_U"....]
    :param test_examples: List of reverse geocoded examples from google maps tagged as
    List of examples predictions tagged where each example is in the for of
    [(w1, class1), (w2, class2),..., (wn, classn)] where class is one of ["B_S", "B_ST", "I_S", "B_U"....]
    :return: statisticts comparision against Order, Completeness, Similarity
    """
    address_similarities = []
    f = None

    if len(dest_file) > 0:
        f = open(dest_file, "a", encoding="utf-8")

    print("Address Standardization Statistics", file=f)
    print("===============================================================", file=f)
    print("Total examples: {}".format(len(src_examples)), file=f)
    print()
    print("O_rdr            CM_plt          Sim             A_sim", file=f)
    print("________________________________________________________", file=f)
    for i, src_example in enumerate(src_examples):
        src_address, src_entities = transform_to_entities2(src_example)
        test_address, test_entities = transform_to_entities2(test_examples[i])
        a_sim, o_rdr, cm_plt, sim = calculate_address_similarity(src_entities, test_entities)
        print("%.4f\t\t\t%.4f\t\t\t%.4f\t\t\t%.4f" % (o_rdr, cm_plt, sim, a_sim), file=f)
        address_similarities.append(a_sim)

    print()
    np_sim = np.asarray(address_similarities, dtype=np.float32)
    avg = np.mean(np_sim, dtype=np.float32)
    print("avg / address_similarity: %.4f" % avg, file=f)


def original_address(prediction):
    return " ".join([word for word, _class in prediction])

def original_labels(prediction):
    return " ".join([_class for word, _class in prediction])


def transform_to_entities2(prediction):
    allowed_entities = {
        "Street_type": {"allowed": True, "values": []},
        "Street_name": {"allowed": True, "values": []},
        "Street_number": {"allowed": True, "values": []},
        "Road": {"allowed": True, "values": []},
        "Road_number": {"allowed": True, "values": []},
        "City_district": {"allowed": True, "values": []},
        "City": {"allowed": True, "values": []},
        "Region": {"allowed": True, "values": []},
        "Country": {"allowed": True, "values": []},
    }

    prev_entity_class = None
    for word, _class in prediction:
        entity_class_code = _class[2:] if len(_class) > 1 else _class
        entity_class = mapping_class[entity_class_code] if entity_class_code not in classes_excluded else None

        if _class[0] == "B" and entity_class and len(allowed_entities[entity_class]["values"]) == 0:
            allowed_entities[entity_class]["values"].append(word)
        elif prev_entity_class and (_class[0] == "O" or (_class[0] == "B" and prev_entity_class != entity_class)):
            allowed_entities[prev_entity_class]["allowed"] = False

        if _class[0] == "I" and entity_class and prev_entity_class and entity_class == prev_entity_class and \
                allowed_entities[prev_entity_class]["allowed"]:
            allowed_entities[entity_class]["values"].append(word)

        prev_entity_class = entity_class

    entities = []
    for key, value in allowed_entities.items():
        entity_value = " ".join(value["values"])
        entities.append((entity_value, key))
    return original_address(prediction), list(filter(lambda x: len(x[0]) > 0, entities))


def capitalize(segment):
    stopwords_list = stopwords.words("spanish")
    return " ".join([entity.capitalize() if entity.lower() not in stopwords_list else entity.lower() for entity in segment.split(" ")])


def standardization(prediction, dictionary={}):
    av_regex = re.compile(r"avenida|avda|ave|avenda|avd|avenue|")
    pje_regex = re.compile(r"pasaje|pj|pje|psaje|psj|psje|pje")
    llaillay = re.compile(r"llay llay")
    nunoa = re.compile(r"uoa|uoua|nuoa|ñuoa|uona")
    camino_regex = re.compile(r"cam|cm")
    region_regex = re.compile(r"13|rm|xiii")

    template_args_dict = {
        "Street_type": "",
        "Street_name": "",
        "Street_number": "",
        "City_district": "",
        "Region": "",
        "Country": "Chile"
    }
    src_address, entities = transform_to_entities2(prediction)
    labels = [_class for w, _class in entities]

    # Earlier exit
    if "Street_name" not in labels and "Road_name" not in labels:
        return capitalize(src_address)

    # Fit arg to dict
    # entities already filtered for first appearance
    for segment, _class in entities:
        if (_class in template_args_dict  and len(template_args_dict[_class]) == 0) or _class == "City":
            key = _class
            value = segment

            # Use Road or Street
            if _class == "Road":
                key = "Street_name"
            elif _class == "Road_number":
                key = "Street_number"

            # Rule for replace CTD with CT
            if _class == "City" and "City_district" not in labels:
                key = "City_district"

            if _class == "Street_number" and re.search(r"[0-9]+(-|_|\||/)[0-9]+", value):
                match = re.search(r"[0-9]+(-|_|\||/)[0-9]+", value)
                value = value.split(match.group(1))[0]

            if _class == "Street_type" and av_regex.search(segment.lower()):
                value = "Av."

            if _class == "Street_type" and pje_regex.search(segment.lower()):
                value = "Pje."

            if _class == "Street_type" and camino_regex.search(segment.lower()):
                value = "Camino"

            if _class == "Region" and region_regex.search(segment.lower()):
                value = capitalize("REGIÓN METROPOLITANA DE SANTIAGO")

            if _class == "City_district" and llaillay.search(value.lower()):
                value = "llaillay"

            if _class == "City_district" and nunoa.search(value.lower()):
                value = "nunoa"

            if _class == "City_district" and "Region" not in labels:
                try:
                    new_value = strip_accents((value.lower()))
                    template_args_dict["Region"] = capitalize(dictionary[new_value]["region"])
                except:
                    pass

            template_args_dict[key] = capitalize(value.lower())

    standard_address = chilean_address_template.substitute(**template_args_dict)
    standard_address = standard_address.strip()
    standard_address = re.sub(r"\s,", ",", standard_address)

    while re.search(r",\s,", standard_address):
        standard_address = re.sub(r",\s,", ",", standard_address)

    while re.search(r",,+", standard_address):
        match = re.search(r"(,,+)", standard_address).group(1)
        standard_address = re.sub(r"{}".format(match), ",", standard_address)

    return standard_address


if __name__ == "__main__":
    import sys
    src_file = sys.argv[1]
    dest_file = sys.argv[2]

    with open("./data/gis/codigos_por_comunas.json", encoding="utf-8") as jsonfile:
        json_data = json.load(jsonfile)

        # Load data
        addresses = joblib.load(src_file)

        # Save results
        f = open(dest_file, "w", encoding="utf-8")

        for address in addresses:
            f.write("{}\n".format(standardization(address, dictionary=json_data)))


