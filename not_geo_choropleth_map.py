# -*- coding: utf-8 -*-
import fiona
import joblib
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
from shapely.geometry import shape

if __name__ == "__main__":
    with fiona.open("./data/gis/Comuna.shp") as map:
        try:
            counter = joblib.load("./data/aproximate_quantity_of_points_in_every_district.out")
        except Exception:
            raise Exception("missing file './data/aproximate_quantity_of_points_in_every_district.out'")

        # Build data
        rows = []
        codes = []
        vmax = max(counter.values())
        vmin = min(counter.values())
        for key, item in map.items():
            codes.append(item['properties']['COMUNA'])
            rows.append({
                'code': item['properties']['COMUNA'],
                'name': item['properties']['DESC_COMUN'],
                'points_qty': counter[item['properties']['COMUNA']],
                'geometry': shape(item['geometry'])
            })

        print(counter)

        # for i in range(1, 16):
        #     if i > 10:
        #         filter_rows = list(filter(lambda x: len(x['code']) == 5 and x['code'][:2] == str(i), rows))
        #         filter_codes = list(filter(lambda x: len(x) == 5 and x[:2] == str(i), codes))
        #         points = [row['points_qty'] for row in filter_rows]
        #     else:
        #         filter_rows = list(filter(lambda x: len(x['code']) == 4 and x['code'][:1] == str(i), rows))
        #         filter_codes = list(filter(lambda x: len(x) == 4 and x[:1] == str(i), codes))
        #         points = [row['points_qty'] for row in filter_rows]
        #
        #     if len(filter_rows) > 0:
        df = pd.DataFrame(rows, index=[codes], columns=['code', 'name', 'points_qty', 'geometry'])
        gdf = gpd.GeoDataFrame(df, geometry='geometry')
        fig, ax = plt.subplots()
        ax.set_aspect('equal')
        gdf.plot(column='points_qty', cmap='Reds', linewidth=0.5, edgecolor='0.5', ax=ax)
        ax.axis('off')

        sm = plt.cm.ScalarMappable(cmap='Reds', norm=plt.Normalize(vmax=vmax, vmin=vmin))
        sm._A = []
        cbar = fig.colorbar(sm)
        fig.savefig("./data/images/not_geo_district_coords_qty.png", dpi=200)
