import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

if __name__ == "__main__":
    gs = gridspec.GridSpec(1, 3)

    fig = plt.figure(figsize=(10,4))
    fig.subplots_adjust(wspace=0.45)
    ax1 = fig.add_subplot(gs[0,0])

    nombres1 = ['After Normalize', 'Only Google Maps']
    datos1 = [100, 100]
    color1 = ['#be2edd', '#6ab04c']
    xx1 = range(len(datos1))

    ax1.bar(xx1, datos1, width=0.3, align='center', color=color1)
    ax1.set_xticks(xx1)
    ax1.set_xticklabels(nombres1)
    plt.title("Dataset 1")
    plt.ylabel("Addresses Geocoded %")

    ax2 = fig.add_subplot(gs[0,1])

    nombres2 = ['After Normalize', 'After Old System']
    datos2 = [100, 50]
    color2 = ['#be2edd', '#f0932b']
    xx2 = range(len(datos2))

    ax2.bar(xx2, datos2, width=0.3, align='center', color=color2)
    ax2.set_xticks(xx2)
    ax2.set_xticklabels(nombres2)
    plt.title("Dataset 2")
    plt.ylabel("Addresses Geocoded %")

    ax3 = fig.add_subplot(gs[0,2])

    nombres3 = ['After Normalize', 'After Old System']
    datos3 = [100, 50]
    color3 = ['#be2edd', '#686de0']
    xx3 = range(len(datos3))

    ax3.bar(xx3, datos3, width=0.3, align='center', color=color3)
    ax3.set_xticks(xx3)
    ax3.set_xticklabels(nombres3)
    plt.title("Dataset 3")
    plt.ylabel("Addresses Geocoded %")

    plt.savefig("./data/images/validation_results.png", dpi=300, bbox_inches='tight')

