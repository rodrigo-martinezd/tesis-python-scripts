import joblib
import json
from collections import Counter
from norman import load_file, strip_accents


def clean(str):
    return strip_accents(str.lower())


if __name__ == "__main__":
    with open("./data/gis/codigos_por_comunas.json", "r", encoding="utf-8") as json_file:
        counter = Counter()
        indexes = []
        data = json.load(json_file)
        addresses = load_file("./addresses_without_georreferences.txt")
        district_keymap = dict()
        capitals = [
            "arica",
            "iquique",
            "antofagasta",
            "copiapo",
            "la serena",
            "valparaiso",
            "rancagua",
            "talca",
            "concepcion",
            "temuco",
            "valdivia",
            "puerto montt",
            "coyhaique",
            "punta arenas",
            "santiago"
        ]

        for i, address in enumerate(addresses):
            for key in data:
                district_keymap[data[key]['code']] = {
                    'name': key,
                    'region_name': data[key]['region'],
                    'region_code': data[key]['region_code']
                }

                if key not in capitals:
                    for district in data[key]['alternatives_names']:
                        if district in clean(address):
                            counter[data[key]['code']] += 1
                            indexes.append(i)

        for i, address in enumerate(addresses):
            if i not in indexes:
                for key in capitals:
                    for district in data[key]['alternatives_names']:
                        if district in clean(address):
                            counter[data[key]['code']] += 1

        joblib.dump(counter, "./data/aproximate_quantity_of_points_in_every_district.out")

        f = open("./data/raw/district_frequency_(not_geo).csv", "w", encoding="utf-8")

        for district_code, count in counter.items():
            name = district_keymap[district_code]['name']
            region = district_keymap[district_code]['region_name']
            region_code = district_keymap[district_code]['region_code']
            f.write("{};{};{};{};{}\n".format(district_code, name, count, region_code, region))

        f.close()

