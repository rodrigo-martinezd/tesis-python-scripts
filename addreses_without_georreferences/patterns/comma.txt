Total	12811

Shapes
========================================

dddd,                                   1420
ddd,                                    1155
dd,                                     885
d,                                      872
AAAAAA,                                 596
AAAAA,                                  573
                                        550
AAAAAAAA,                               518
AAAAAAA,                                442
A,                                      405

Compact Shapes
========================================
d,                                      4567
A,                                      3899
Aa,                                     802
                                        550
A#d,                                    494
a,                                      474
A#A,                                    428
A#,                                     135
Ad,                                     93
d#A,                                    81

